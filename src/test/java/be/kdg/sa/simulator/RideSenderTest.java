package be.kdg.sa.simulator;


import be.kdg.sa.simulator.services.CSVReaderService;
import be.kdg.sa.simulator.util.StationRideHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RideSenderTest {
    @Autowired
    private CSVReaderService csvReaderService;

    @Test
    public void startFreeRide() {
        csvReaderService.readFreeRide();
    }

    @Test
    public void startStationRide() {
        csvReaderService.readStationRides();
    }

}
