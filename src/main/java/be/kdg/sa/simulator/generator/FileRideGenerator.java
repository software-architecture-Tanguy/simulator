package be.kdg.sa.simulator.generator;


import be.kdg.sa.simulator.model.FreeRideEvent;
import be.kdg.sa.simulator.model.StationRideEvent;
import be.kdg.sa.simulator.util.FileIO;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;

@Slf4j
@NoArgsConstructor
public class FileRideGenerator implements MessageGenerator {

    private FileIO fileIO;
    private LinkedList<FreeRideEvent> freeRideEvents;
    private LinkedList<StationRideEvent> stationRideEvents;

    public FileRideGenerator(FileIO fileIO) { //TODO change BASE thing like this, with fancy
        this.fileIO = fileIO;
        this.freeRideEvents = new LinkedList<>();
        this.stationRideEvents = new LinkedList<>();
    }

    @Override
    public Object generate() {
        freeRideEvents = fileIO.readFreeRide();
        stationRideEvents = fileIO.readStationRide();
        return freeRideEvents;
    }

}
