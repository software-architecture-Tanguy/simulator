package be.kdg.sa.simulator.generator;

import be.kdg.sa.simulator.model.SensorMessage;
import be.kdg.sa.simulator.model.SensorType;
import be.kdg.sa.simulator.model.dto.SensorSettingDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.time.LocalDateTime;

@NoArgsConstructor
@Slf4j
@Data
@EnableConfigurationProperties(RandomSensorMessageGenerator.class)
@ConfigurationProperties(prefix = "gen")
public class RandomSensorMessageGenerator implements MessageGenerator{
    @Autowired
    private RandomValueGenerator rvg;
    private int genTimePeriod;
    private int delay;
    private int randomVariation;
    private int xCoordRange;
    private int yCoordRange;


    @Override
    public SensorMessage generate() {
        try{
            Thread.sleep(rvg.generateDelay(delay, randomVariation));
            SensorType sensorType =  rvg.randomSensorType();
            return new SensorMessage(LocalDateTime.now(), rvg.generateCoord(50, xCoordRange), rvg.generateCoord(3, yCoordRange), sensorType, rvg.generateSensorValue(sensorType));
        } catch (InterruptedException e) {
            log.warn("An error occured when trying to generate messages!");
        }
        return null;
    }

    public SensorMessage generateWithParams(SensorSettingDTO sensorSettingDTO) {
        try {
            Thread.sleep(rvg.generateDelay(sensorSettingDTO.getDelay(), sensorSettingDTO.getRandomVariation()));
            SensorType sensorType= rvg.randomSensorType();
            return new SensorMessage(LocalDateTime.now(), rvg.generateCoord(50, sensorSettingDTO.getXCoordRange()), rvg.generateCoord(3, sensorSettingDTO.getYCoordRange()), sensorType, rvg.generateSensorValue(sensorType));
        } catch (InterruptedException e) {
           log.warn("An error occured when trying to generate sensormessages!");
        }
        return null;
    }
}

