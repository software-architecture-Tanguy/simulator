package be.kdg.sa.simulator.generator;

public interface MessageGenerator<T> {
    T generate() throws InterruptedException;
}
