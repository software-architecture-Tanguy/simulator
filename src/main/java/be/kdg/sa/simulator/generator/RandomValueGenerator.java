package be.kdg.sa.simulator.generator;

import be.kdg.sa.simulator.model.SensorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Random;

@Component
public class RandomValueGenerator {
    private final Random random = new Random();
    @Autowired
    private Environment environment;

    public int generateDelay(int delay, int variation) {
        int upperBound = delay + variation;
        int lowerBound = delay - variation;
        return random.nextInt(upperBound - lowerBound) + lowerBound;
    }

    public double generateCoord(int lowerbound, int upperbound) {
        return lowerbound + random.nextDouble() * (upperbound - lowerbound);
    }


    public double generateSensorValue(SensorType sensorType) {
        int lowerbound;
        int upperbound;
        switch (sensorType) {
            case CO2:
                lowerbound = Integer.parseInt(Objects.requireNonNull(environment.getProperty("lowerboundC02")));
                upperbound = Integer.parseInt(Objects.requireNonNull(environment.getProperty("upperboundCO2")));
                return (random.nextInt(upperbound - lowerbound) + lowerbound);
            case FINE_DUST:
                lowerbound = Integer.parseInt(Objects.requireNonNull(environment.getProperty("lowerboundFineDust")));
                upperbound = Integer.parseInt(Objects.requireNonNull(environment.getProperty("upperboundFineDust")));
                return random.nextInt(upperbound - lowerbound) + lowerbound;
            default:
                lowerbound = 0;
                upperbound = 1000;
                return random.nextInt(upperbound - lowerbound) + lowerbound;
        }
    }

    public SensorType randomSensorType() {
        int pick = random.nextInt(SensorType.values().length);
        return SensorType.values()[pick];
    }

}
