package be.kdg.sa.simulator.simulation;

import be.kdg.sa.simulator.generator.MessageGenerator;
import be.kdg.sa.simulator.model.SensorMessage;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class MessageFactory {

    private final MessageGenerator messageGenerator;

    SensorMessage getSensorMessage() throws InterruptedException {
        SensorMessage sensorMessage = (SensorMessage) messageGenerator.generate();
        log.info("A new sensormessage was generated: " + sensorMessage.toString());
            return sensorMessage;
        }
    }

