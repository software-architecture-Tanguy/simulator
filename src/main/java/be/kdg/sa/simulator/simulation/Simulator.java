package be.kdg.sa.simulator.simulation;

import be.kdg.sa.simulator.generator.RandomSensorMessageGenerator;
import be.kdg.sa.simulator.messengers.Messenger;
import be.kdg.sa.simulator.model.SensorMessage;
import be.kdg.sa.simulator.model.dto.SensorSettingDTO;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "sim")
@Data
@Component
public class Simulator {
    @Autowired
    private final MessageFactory messageFactory;
    private final Messenger messenger;
    private int genTimePeriod;
    //private final RandomSensorMessageGenerator randomSensorMessageGenerator;


    @Async("asyncExecutor")
    public void sendMessage() throws InterruptedException {
        long start = System.currentTimeMillis();
        // converts the minutes from the application.properties to milliseconds
        long end = start + (genTimePeriod * 60 * 1000);
        while (System.currentTimeMillis() < end) {
            SensorMessage sensorMessage = messageFactory.getSensorMessage();
            messenger.sendMessage(sensorMessage);
        }
    }

/*    @Async("asyncExecutor")
    public void sendParameterMessage(SensorSettingDTO sensorSettingDTO) {
        long start = System.currentTimeMillis();
        // converts the minutes from the application.properties to milliseconds
        long end = start + (genTimePeriod * 60 * 1000);
        while (System.currentTimeMillis() < end) {
            SensorMessage sensorMessage = randomSensorMessageGenerator.generateWithParams(sensorSettingDTO);
            messenger.sendMessage(sensorMessage);
        }
    }*/
}
