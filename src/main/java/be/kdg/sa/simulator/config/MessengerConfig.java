package be.kdg.sa.simulator.config;

import be.kdg.sa.simulator.messengers.CommandLineMessenger;
import be.kdg.sa.simulator.messengers.QueueMessenger;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class MessengerConfig {
    private final RabbitTemplate rabbitTemplate;
    private final Queue queue;
    private final ObjectMapper objectMapper;

    @Bean
    @ConditionalOnProperty(name = "messenger", havingValue = "rabbit")
    public QueueMessenger queueMessenger() { return new QueueMessenger(rabbitTemplate, queue, objectMapper);}

    @Bean
    @ConditionalOnProperty(name = "messenger", havingValue = "clm")
    public CommandLineMessenger commandLineMessenger() { return new CommandLineMessenger();}

}
