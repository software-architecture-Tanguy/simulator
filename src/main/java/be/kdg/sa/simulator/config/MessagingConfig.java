package be.kdg.sa.simulator.config;

import be.kdg.sa.simulator.generator.FileRideGenerator;
import be.kdg.sa.simulator.generator.MessageGenerator;
import be.kdg.sa.simulator.generator.RandomSensorMessageGenerator;
import be.kdg.sa.simulator.generator.RandomValueGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessagingConfig {

    @Bean
    @ConditionalOnProperty(name = "modus", havingValue = "sensor")
    public MessageGenerator randomSensorMessageGenerator() {
        return new RandomSensorMessageGenerator();
    }

    @Bean
    @ConditionalOnProperty(name = "modus", havingValue = "ride")
    public MessageGenerator fileGenerator() {
        return new FileRideGenerator();
    }

}

