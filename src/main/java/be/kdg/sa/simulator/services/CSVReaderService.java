package be.kdg.sa.simulator.services;

import be.kdg.sa.simulator.util.FileIO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Service
@AllArgsConstructor
public class CSVReaderService {
    private final FileIO fileIO;

    public void readStationRides() {
        fileIO.readStationRide();
    }

    public void readFreeRide() {
        fileIO.readFreeRide();
    }

    public void readCsvWeb(MultipartFile file) { fileIO.readStationCsv(file);}
}
