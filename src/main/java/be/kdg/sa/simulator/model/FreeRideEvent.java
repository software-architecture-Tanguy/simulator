package be.kdg.sa.simulator.model;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FreeRideEvent {
    @CsvBindByPosition(position = 0)
    private String event;
    @CsvBindByPosition(position = 1)
    private Long userID;
    @CsvBindByPosition(position = 2)
    private Long vehicleId;
    private LocalDateTime timestamp = LocalDateTime.now();
    @CsvBindByPosition(position = 4)
    private double xCoord;
    @CsvBindByPosition(position = 5)
    private double yCoord;
    @CsvBindByPosition(position = 6)
    private Long stationId;
    @CsvBindByPosition(position = 7)
    private String vehicleType;
    @CsvBindByPosition(position = 8)
    private long delayMillis;
}
