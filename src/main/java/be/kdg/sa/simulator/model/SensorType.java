package be.kdg.sa.simulator.model;

public enum SensorType {
    CO2, FINE_DUST
}
