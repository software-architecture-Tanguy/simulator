package be.kdg.sa.simulator.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SensorMessage {
    private LocalDateTime timestamp;
    private double xCoord;
    private double yCoord;
    private SensorType sensorType;
    private double value;
}
