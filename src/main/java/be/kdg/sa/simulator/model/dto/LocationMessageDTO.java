package be.kdg.sa.simulator.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class LocationMessageDTO {
    private Long vehicleId;
    private double xCoord;
    private double yCoord;
    private LocalDateTime timestamp;
}
