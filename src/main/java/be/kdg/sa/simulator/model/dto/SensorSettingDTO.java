package be.kdg.sa.simulator.model.dto;

import lombok.Data;

@Data
public class SensorSettingDTO {
    private int delay;
    private int randomVariation;
    private int xCoordRange;
    private int yCoordRange;
}
