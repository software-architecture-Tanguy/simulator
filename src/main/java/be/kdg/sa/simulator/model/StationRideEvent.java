package be.kdg.sa.simulator.model;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class StationRideEvent {
        @CsvBindByPosition(position = 0)
        private String event;
        @CsvBindByPosition(position = 1)
        private Long userId;
        @CsvBindByPosition(position = 2)
        private Long stationId;
        @CsvBindByPosition(position = 3)
        private Long lockId;
        @CsvBindByPosition(position = 4)
        private long delayMillis;


}
