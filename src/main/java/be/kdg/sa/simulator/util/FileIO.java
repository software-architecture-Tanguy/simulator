package be.kdg.sa.simulator.util;

import be.kdg.sa.simulator.model.FreeRideEvent;
import be.kdg.sa.simulator.model.StationRideEvent;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.LinkedList;


@Slf4j
@Component
public class FileIO {//TODO too 'simple' & 'lowlevel' ?

    @Value("${file.pathFree}")
    private String freeRideFile;
    private final StationRideHandler stationRideHandler;
    private final FreeRideHandler freeRideHandler;

    public FileIO(StationRideHandler stationRideHandler, FreeRideHandler freeRideHandler) {
        this.stationRideHandler = stationRideHandler;
        this.freeRideHandler = freeRideHandler;
    }

    public LinkedList<FreeRideEvent> readFreeRide() {
        LinkedList<FreeRideEvent> events = new LinkedList<>();
        try (
                Reader reader = Files.newBufferedReader(Paths.get("../simulator/src/main/resources/freeRide.csv"));
        ) {
            CsvToBean csvToBean = new CsvToBeanBuilder(reader)
                    .withType(FreeRideEvent.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .withSeparator(';')
                    .withSkipLines(1)
                    .build();

            Iterator csvUserIterator = csvToBean.iterator();

            while (csvUserIterator.hasNext()) {
                FreeRideEvent freeRideEvent = (FreeRideEvent) csvUserIterator.next();
                freeRideHandler.handleFreeRideEvent(freeRideEvent);
                events.add(freeRideEvent);
            }
        } catch (IOException | InterruptedException e) {
            log.warn("An error occured when trying to read the CSV file!");
        }
        return events;
    }


    public LinkedList<StationRideEvent> readStationRide() {
        LinkedList<StationRideEvent> stationEvents = new LinkedList<>();
        try (
                Reader reader = Files.newBufferedReader(Paths.get("../simulator/src/main/resources/stationRide.csv"));
        ) {
            CsvToBean csvToBean = new CsvToBeanBuilder(reader)
                    .withType(StationRideEvent.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .withSeparator(';')
                    .withSkipLines(1)
                    .build();

            Iterator csvUserIterator = csvToBean.iterator();
            while (csvUserIterator.hasNext()) {
                StationRideEvent stationRideEvent = (StationRideEvent) csvUserIterator.next();
                stationRideHandler.handleStationRideEvent(stationRideEvent);
                stationEvents.add(stationRideEvent);
            }
        } catch (IOException | InterruptedException e) {
            log.warn("An error occured when trying to read the CSV file!");
        }
        return stationEvents;
    }

    @Async("asyncExecutor")
    public LinkedList<StationRideEvent> readStationCsv(MultipartFile file) {
        LinkedList<StationRideEvent> stationEvents = new LinkedList<>();
        try (
                Reader reader = new InputStreamReader(file.getInputStream());
        ) {
            CsvToBean csvToBean = new CsvToBeanBuilder(reader)
                    .withType(StationRideEvent.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .withSeparator(';')
                    .withSkipLines(1)
                    .build();

            Iterator csvUserIterator = csvToBean.iterator();

            while (csvUserIterator.hasNext()) {
                StationRideEvent stationRideEvent = (StationRideEvent) csvUserIterator.next();
                stationRideHandler.handleStationRideEvent(stationRideEvent);
                stationEvents.add(stationRideEvent);
            }
        } catch (IOException | InterruptedException ex) {
            log.warn("An error occured when trying to read the CSV file!");
        }
        return stationEvents;
    }
}
