package be.kdg.sa.simulator.util;

import be.kdg.sa.simulator.messengers.Messenger;
import be.kdg.sa.simulator.model.FreeRideEvent;
import be.kdg.sa.simulator.model.dto.LocationMessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.time.LocalDateTime;

@Component
@Slf4j
public class FreeRideHandler {

    private RestTemplate restTemplate;
    @Value("${baseurl}")
    private String baseurl;
    private Messenger messenger;

    public FreeRideHandler(RestTemplate restTemplate, Messenger messenger) {
        this.restTemplate = restTemplate;
        this.messenger = messenger;
    }

    public void handleFreeRideEvent(FreeRideEvent freeRideEvent) throws InterruptedException {
        switch (freeRideEvent.getEvent()) {
            case "unlock":
                Boolean unlockResult = unlockFreeVehicle(freeRideEvent.getUserID(), freeRideEvent.getVehicleId());
                log.info("Unlock vehicle status: " + unlockResult);
                Thread.sleep(freeRideEvent.getDelayMillis());
                break;
            case "location":
                handleLocation(freeRideEvent.getVehicleId(), freeRideEvent.getXCoord(), freeRideEvent.getYCoord());
                Thread.sleep(freeRideEvent.getDelayMillis());
                break;
            case "lock":
                Boolean lockResult =  lockFreeVehicle(freeRideEvent.getVehicleId(), freeRideEvent.getUserID());
                log.info("Lock vehicle status: " + lockResult);
                Thread.sleep(freeRideEvent.getDelayMillis());
                break;
            case "nearest":
                Long nearResult = findNearest(freeRideEvent.getVehicleId());
                log.info("Nearest Vehicle is: " + nearResult);
                Thread.sleep(freeRideEvent.getDelayMillis());
                break;
        }
    }

    public Boolean unlockFreeVehicle(Long userId, Long vehicleId) {
        String url = baseurl + "/free/unlock/" + userId + "/" + vehicleId;
        return restTemplate.getForObject(url, Boolean.class);
    }

    public void handleLocation(Long vehicleId, double xCoord, double yCoord) {
        messenger.sendLocationMessage(new LocationMessageDTO(vehicleId, xCoord, yCoord, LocalDateTime.now()));
    }

    public Boolean lockFreeVehicle(Long vehicleId, Long userId) {
        String url = baseurl + "/free/lock/" + userId + "/" + vehicleId;
        return restTemplate.getForObject(url, Boolean.class);
    }

    public Long findNearest(Long vehicleId) {
        String url = baseurl + "/free/nearest" + "/" + vehicleId;
        return restTemplate.getForObject(url, Long.class);
    }
}
