package be.kdg.sa.simulator.util;

import be.kdg.sa.simulator.model.StationRideEvent;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class StationRideHandler {
    @Value("${baseurl}")
    private String baseurl;
    private RestTemplate restTemplate;

    public StationRideHandler(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public void handleStationRideEvent(StationRideEvent stationRideEvent) throws InterruptedException {
        switch (stationRideEvent.getEvent()) {
            case "unlock":
                Long unlockedId = unlockStationVehicle(stationRideEvent.getUserId(), stationRideEvent.getStationId());
                log.info("Lock with id: " + unlockedId + " has been unlocked");
                Thread.sleep(stationRideEvent.getDelayMillis());
                break;
            case "lock":
                lockStationVehicle(stationRideEvent.getUserId(), stationRideEvent.getLockId());
                log.info("Lock with id: " + stationRideEvent.getLockId() + " has been locked");
                Thread.sleep(stationRideEvent.getDelayMillis());
                break;
            case "get_free_locks":
                getFreeLocks(stationRideEvent.getStationId());
                Thread.sleep(stationRideEvent.getDelayMillis());
                break;
        }
    }


    public Long unlockStationVehicle(Long userid, Long stationId) {
        String url = baseurl + "/station/unlock/" + userid + "/" + stationId;
        return restTemplate.getForObject(url, Long.class);
    }

    public void getFreeLocks(Long stationId) {
        String url = baseurl + "/lock/" + stationId;
        List<Integer> response = restTemplate.getForObject(
                url,
                List.class);
        for (Integer id : response) {
            log.info(id.toString());
        }
    }

    public void lockStationVehicle(Long userid, Long lockId) {
        String url = baseurl + "/station/lock/" + userid + "/" + lockId;
        Map<String, Long> params = new HashMap<>();
        params.put("userid", userid);
        params.put("lockId", lockId);
        restTemplate.put(url, params);
    }
}
