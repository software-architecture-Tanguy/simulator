package be.kdg.sa.simulator.web;

import be.kdg.sa.simulator.model.dto.SensorSettingDTO;
import be.kdg.sa.simulator.services.CSVReaderService;
import be.kdg.sa.simulator.simulation.Simulator;
import be.kdg.sa.simulator.util.FileIO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;

@AllArgsConstructor
@Controller
@Slf4j
@RequestMapping("/")
public class SimulatorController {
    private final Simulator simulator;
    private final CSVReaderService csvReaderService;

    @PostMapping("/sensor/start")
    public ModelAndView startRandomMessageGeneration() throws InterruptedException {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("started");
        simulator.sendMessage();
        return modelAndView;
    }

  /*  @PostMapping("/parameters/start")
    public ModelAndView startParametersGeneration(SensorSettingDTO sensorSettingDTO) {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("started");
        simulator.sendParameterMessage(sensorSettingDTO);
        return modelAndView;
    }
*/
    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file) {
        csvReaderService.readCsvWeb(file);
        return "started";
    }

    @GetMapping("/csv")
    public String csvPage() { return "csvUpload";}

    @GetMapping("/start")
    public String welcome() {
        return "index";
    }

    @GetMapping("/rides")
    public String rides() {
        return "rides";
    }

    @GetMapping("/parameters")
    public ModelAndView parameters(SensorSettingDTO sensorSettingDTO) {
        return new ModelAndView("parameters", "sensorSettingDTO", sensorSettingDTO);
     }

    @PostMapping("/rides/start")
    public ModelAndView startRideSimulation() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("ride_started");
        csvReaderService.readFreeRide();
        csvReaderService.readStationRides();
        return modelAndView;
    }

}
