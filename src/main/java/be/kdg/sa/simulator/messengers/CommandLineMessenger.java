package be.kdg.sa.simulator.messengers;

import be.kdg.sa.simulator.model.SensorMessage;
import be.kdg.sa.simulator.model.dto.LocationMessageDTO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommandLineMessenger implements Messenger{

    @Override
    public void sendMessage(SensorMessage sensorMessage) {
        log.info(sensorMessage.toString());
    }

    @Override
    public void sendLocationMessage(LocationMessageDTO locationMessageDTO) {
        log.info(locationMessageDTO.toString());
    }
}
