package be.kdg.sa.simulator.messengers;

import be.kdg.sa.simulator.model.SensorMessage;
import be.kdg.sa.simulator.model.dto.LocationMessageDTO;

public interface Messenger {

    void sendMessage(SensorMessage sensorMessage);

    void sendLocationMessage(LocationMessageDTO locationMessageDTO);
}
