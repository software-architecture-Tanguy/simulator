package be.kdg.sa.simulator.messengers;

import be.kdg.sa.simulator.model.SensorMessage;
import be.kdg.sa.simulator.model.dto.LocationMessageDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

@Slf4j
@AllArgsConstructor
public class QueueMessenger implements Messenger{
    private final RabbitTemplate rabbitTemplate;
    private final Queue queue;
    private final ObjectMapper objectMapper;


    @Override
    public void sendMessage(SensorMessage sensorMessage) {
        try {
            this.rabbitTemplate.convertAndSend(queue.getName(), objectMapper.writeValueAsString(sensorMessage));
            log.info("A Sensormessage was sent to the queue");
        } catch (JsonProcessingException e) {
            log.warn("Failed to process Sensormessage");
        }
    }

    @Override
    public void sendLocationMessage(LocationMessageDTO locationMessageDTO) {
        try {
            this.rabbitTemplate.convertAndSend(queue.getName(), objectMapper.writeValueAsString(locationMessageDTO));
            log.info("Locationmessage was sent to the queue" + objectMapper.writeValueAsString(locationMessageDTO));
        } catch (JsonProcessingException e) {
            log.warn("Unable to process location to JSON!");
        }
    }
}
